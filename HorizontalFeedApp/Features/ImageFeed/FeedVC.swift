//
//  ViewController.swift
//  HorizontalFeedApp
//
//  Created by Yura Buchin on 11/05/2019.
//  Copyright © 2019 Yura Buchin. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class FeedViewController: UIViewController {
    private var feedPageXOffset: CGFloat = 0.0

    private let viewModel = FeedViewModel(imageUseCases: imageUseCases)
    private let disposeBag = DisposeBag()

    private let imageCollectionView = createFeedCollectionView()

    override func viewDidLoad() {
        super.viewDidLoad()
        setAutolayout()

        FeedViewController.configureFeedCollectionView(
                self,
                imageCollectionView,
                viewModel,
                disposeBag
        )

        viewModel.intentions.onNext(.viewDidLoad)
    }
}

extension FeedViewController: Autolayouted {
    var viewHierarchy: ViewHierarchy {
        return .view(
                view,
                subHierarchy: [
                    .view(imageCollectionView,
                            subHierarchy: nil),
                ]
        )
    }

    var autolayout: [Autolayouted.Autolayout] {
        return [
            (view: imageCollectionView, layout: {
                $0.center.equalToSuperview()
                $0.size.equalToSuperview()
            }),
        ]
    }
}

extension FeedViewController {
    private static func createFeedCollectionView() -> UICollectionView {
        let view = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        if let flowLayout = view.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0
        }
        view.backgroundColor = .white
        view.register(ImageCell.self, forCellWithReuseIdentifier: ImageCell.cellIdentifier)
        view.register(AdCell.self, forCellWithReuseIdentifier: AdCell.cellIdentifier)
        view.isPagingEnabled = true
        return view
    }
}

extension FeedViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)
    }

    func scrollViewWillEndDragging(_ scrollView: UIScrollView,
                                   withVelocity velocity: CGPoint,
                                   targetContentOffset: UnsafeMutablePointer<CGPoint>) {

        let targetOffset = targetContentOffset.pointee

//        if targetOffset.x == feedPageXOffset {
//            print("page will not change")
//        } else if targetOffset.x < feedPageXOffset {
//            print("scroll view will page left")
//        } else if targetOffset.x > feedPageXOffset {
//            print("scroll view will page right")
//        }

        feedPageXOffset = targetOffset.x

        let index = Int(feedPageXOffset / scrollView.frame.width)
        viewModel.intentions.onNext(.currentFeedElementPositionChanged(index))
    }
}

extension FeedViewController {
    static private func configureFeedCollectionView(_ uiCollectionViewDelegateFlowLayout: UICollectionViewDelegateFlowLayout,
                                                    _ feedCollectionView: UICollectionView,
                                                    _ viewModel: FeedViewModel,
                                                    _ disposeBag: DisposeBag) {

        feedCollectionView.delegate = uiCollectionViewDelegateFlowLayout

        let dataSource = RxCollectionViewSectionedReloadDataSource<SectionOfFeedElement>(configureCell: { _, collectionView, indexPath, item in
            switch item {
            case .images(let imageUrl, let imageText):
                let cell = collectionView.dequeueReusableCell(
                        withReuseIdentifier: ImageCell.cellIdentifier, for: indexPath) as! ImageCell
                cell.set(imageText: imageText, imageUrl: imageUrl)
                return cell
            case .webView:
                let cell = collectionView.dequeueReusableCell(
                        withReuseIdentifier: AdCell.cellIdentifier, for: indexPath) as! AdCell
                return cell
            }
        })

        viewModel.viewState
                .map { currentState in
                    currentState.feedElements
                }
                .distinctUntilChanged()
                .map { feedElements in
                    [SectionOfFeedElement(items: feedElements)]
                }
                .bind(to: feedCollectionView.rx.items(dataSource: dataSource))
                .disposed(by: disposeBag)

        viewModel.viewState
                .filter { currentState in
                    return currentState.isNeedScrollToPosition
                }
                .map { currentState in
                    currentState.currentFeedElementPosition
                }
                .distinctUntilChanged()
                .subscribe { event in
                    if let currentFeedElementPosition = event.element {
                        feedCollectionView.scrollToItem(at: IndexPath(row: currentFeedElementPosition, section: 0),
                                at: .centeredHorizontally,
                                animated: false)
                    }
                }
                .disposed(by: disposeBag)

        viewModel.viewState
                .distinctUntilChanged { (prevState: FeedViewState, newState: FeedViewState) -> Bool in
                    return prevState.feedElements == newState.feedElements
                }
                .subscribe(onNext: { currentViewState in
                    feedCollectionView.reloadData()

                    if currentViewState.isNeedScrollToPosition {
                        let currentFeedElementPosition = currentViewState.currentFeedElementPosition
                        feedCollectionView.scrollToItem(at: IndexPath(row: currentFeedElementPosition, section: 0),
                                at: .centeredHorizontally,
                                animated: false)
                    }
                })
                .disposed(by: disposeBag)
    }
}

struct SectionOfFeedElement {
    var items: [FeedElement]
}

extension SectionOfFeedElement: SectionModelType {
    init(original: SectionOfFeedElement, items: [FeedElement]) {
        self = original
        self.items = items
    }
}
