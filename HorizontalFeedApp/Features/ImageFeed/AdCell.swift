//
//  File.swift
//  HorizontalFeedApp
//
//  Created by Yura Buchin on 11/05/2019.
//  Copyright © 2019 Yura Buchin. All rights reserved.
//

import WebKit

class AdCell: UICollectionViewCell {
    static let cellIdentifier = "ADCell"

    private let webView = createWebView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        setAutolayout()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension AdCell {
    private static let adUrls = ["https://habr.com/ru/all/", "https://habr.com/ru/news/"]

    private static func createWebView() -> WKWebView {
        let view = WKWebView()
        view.load(URLRequest(url: URL(string: adUrls[Int.random(in: 0..<adUrls.count)])!))
        return view
    }
}

extension AdCell: Autolayouted {
    var viewHierarchy: ViewHierarchy {
        return .view(
                contentView,
                subHierarchy: [
                    .view(webView, subHierarchy: nil),
                ]
        )
    }

    var autolayout: [Autolayouted.Autolayout] {
        return [
            (view: webView, layout: {
                $0.center.equalToSuperview()
                $0.size.equalToSuperview()
            }),
        ]
    }
}
