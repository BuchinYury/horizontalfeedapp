//
//  PartnersViewModel.swift
//  HorizontalFeedApp
//
//  Created by Yura Buchin on 11/05/2019.
//  Copyright © 2019 Yura Buchin. All rights reserved.
//

import Foundation
import RxSwift
import Moya
import GiphyCoreSDK

enum FeedElement: Equatable {
    case images(String, String)
    case webView
}

struct FeedViewState {
    let feedElements: [FeedElement]
    let currentFeedElementPosition: Int
    let isNeedScrollToPosition: Bool

    init(feedElements: [FeedElement] = [],
         currentFeedElementPosition: Int = 0,
         isNeedScrollToPosition: Bool = false) {

        self.feedElements = feedElements
        self.currentFeedElementPosition = currentFeedElementPosition
        self.isNeedScrollToPosition = isNeedScrollToPosition
    }

    func copy(feedElements: [FeedElement]? = nil,
              currentFeedElementPosition: Int? = nil,
              isNeedScrollToPosition: Bool = false) -> FeedViewState {

        return FeedViewState(feedElements: feedElements ?? self.feedElements,
                currentFeedElementPosition: currentFeedElementPosition ?? self.currentFeedElementPosition,
                isNeedScrollToPosition: isNeedScrollToPosition)
    }
}

enum FeedIntentions {
    case viewDidLoad, retryLoadImagesButtonPressed, currentFeedElementPositionChanged(Int)
}

class FeedViewModel {
    typealias StateReducer = (FeedViewState) -> FeedViewState

    let viewState: Observable<FeedViewState> = PublishSubject()

    let intentions = PublishSubject<FeedIntentions>()
    private let disposeBag = DisposeBag()

    init(initialState: FeedViewState = FeedViewState(),
         imageUseCases: ImageUseCases) {
        let viewDidLoad = intentions
                .filter { intention in
                    switch intention {
                    case .viewDidLoad: return true
                    default: return false
                    }
                }
                .flatMap { _ in
                    imageUseCases.getRandomImageUrlFromGPO()
                            .map { imgUrls -> StateReducer in
                                { (currentState: FeedViewState) -> FeedViewState in
                                    var newFeedElements = currentState.feedElements

                                    for imgUrl in imgUrls {
                                        newFeedElements.append(.images(imgUrl, "Random Int \(Int.random(in: 0...1000))"))
                                    }

                                    return currentState.copy(feedElements: newFeedElements)
                                }
                            }
                }

        let repeater = Observable<Int>.interval(5.0, scheduler: MainScheduler.instance)
                .map { _ in
                    { (currentState: FeedViewState) -> FeedViewState in
                        var newFeedElements = currentState.feedElements

                        let insertIndex = Int.random(in: 0...newFeedElements.count)

                        newFeedElements.insert(.webView, at: insertIndex)

                        if currentState.currentFeedElementPosition < insertIndex {
                            return currentState.copy(feedElements: newFeedElements)
                        } else {
                            return currentState.copy(feedElements: newFeedElements,
                                    currentFeedElementPosition: currentState.currentFeedElementPosition + 1,
                                    isNeedScrollToPosition: true)
                        }
                    }
                }
        let currentFeedElementPositionChanged = intentions
                .map { intention -> Int? in
                    guard case .currentFeedElementPositionChanged(let currentFeedElementPosition) = intention else {
                        return nil
                    }
                    return currentFeedElementPosition
                }
                .filter { currentFeedElementPosition in
                    return currentFeedElementPosition != nil
                }
                .map { currentFeedElementPosition -> StateReducer in
                    { (currentState: FeedViewState) -> FeedViewState in
                        return currentState.copy(currentFeedElementPosition: currentFeedElementPosition,
                                isNeedScrollToPosition: false)
                    }
                }

        let stateReducers = [viewDidLoad, repeater, currentFeedElementPositionChanged]

        Observable.merge(stateReducers)
                .scan(initialState, accumulator: { (currentState: FeedViewState, stateReducer: StateReducer) -> FeedViewState in
                    return stateReducer(currentState)
                })
                .subscribe(onNext: { (newState: FeedViewState) -> Void in
                    (self.viewState as? PublishSubject)?.onNext(newState)
                })
                .disposed(by: disposeBag)
    }
}
