//
//  File.swift
//  HorizontalFeedApp
//
//  Created by Yura Buchin on 11/05/2019.
//  Copyright © 2019 Yura Buchin. All rights reserved.
//

import UIKit
import Kingfisher

class ImageCell: UICollectionViewCell {
    static let cellIdentifier = "ImageFeedCell"

    private var imageUrl: String? {
        didSet {
            if let imageUrl = imageUrl {
                if var urlComponent = URLComponents(string: imageUrl) {
                    urlComponent.scheme = "https"
                    if let imageUrl = urlComponent.string {
                        imageView.kf.setImage(with: URL(string: imageUrl))
                    }
                }
            } else {
                imageView.image = nil
            }
        }
    }
    private var imageText: String? {
        didSet {
            cellLabel.text = imageText
        }
    }

    private let cellLabel: UILabel = UILabel()
    private let imageView = createImage()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        setAutolayout()
    }

    func set(imageText: String, imageUrl: String) {
        self.imageText = imageText
        self.imageUrl = imageUrl
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ImageCell {
    private static func createImage() -> UIImageView {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }
}

extension ImageCell: Autolayouted {
    var viewHierarchy: ViewHierarchy {
        return .view(
                contentView,
                subHierarchy: [
                    .view(cellLabel, subHierarchy: nil),
                    .view(imageView, subHierarchy: nil),
                ]
        )
    }

    var autolayout: [Autolayouted.Autolayout] {
        return [
            (view: imageView, layout: {
                $0.center.equalToSuperview()
                $0.size.equalToSuperview()
            }),
            (view: cellLabel, layout: {
                $0.left.equalToSuperview()
                $0.top.equalToSuperview().offset(16)
                $0.right.equalToSuperview()
            }),
        ]
    }
}
