//
//  ImageUseCases.swift
//  HorizontalFeedApp
//
//  Created by Yura Buchin on 11/05/2019.
//  Copyright © 2019 Yura Buchin. All rights reserved.
//

import RxSwift
import GiphyCoreSDK

let imageUseCases = ImageUseCases(gphClient: GPHClient(apiKey: "ilDltK5lu0JzITZkc8qdw3hgiSL0N3gs"))

class ImageUseCases {
    private let gphClient: GPHClient

    init(gphClient: GPHClient) {
        self.gphClient = gphClient
    }

    func getRandomImageUrlFromGPO() -> Observable<[String]> {
        return Observable<[String]>.create { (observer) in
            self.gphClient.search("cats") { (response, error) in
                if let response = response, let data = response.data {
                    observer.onNext(data.map { (result) -> String? in
                                result.images?.original?.gifUrl
                            }
                            .filter { gifUrl in
                                return gifUrl != nil
                            } as! [String])
                }
            }

            return Disposables.create()
        }
    }
}

